﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestApp
{
    public static class FileManager
    {
        public static List<Node> ReadFile(string fileName)
        {
            List<Node> nodes = new List<Node>();
            using (var streamReader = File.OpenText(fileName))
            {
                var lines = streamReader.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in lines)
                {
                    string pattern = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{4}";
                    MatchCollection dateString = Regex.Matches(line, pattern);
                    var date = GetDate(dateString[0].ToString());
                    var result = Regex.Matches(line, @"\w(?<!\d)[\w'-]*");
                    Node node = new Node(result[0].ToString(), result[1].ToString(), date);
                    nodes.Add(node);
                }
                // Process line
            }
            return nodes;
        }

        private static DateTime GetDate(string date)
        {
            var res = DateTime.Parse(date);
            return res;
        }
    }
}
