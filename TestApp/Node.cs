﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public class Node
    {
        public string StartNode { get; set; }
        public int CountStart { get; set; }

        public string EndNode { get; set; }
        public int CountEnd { get; set; }

        public int Edge { get; set; }
        public DateTime Date { get; set; }

        public Node(string start, string end, DateTime date)
        {
            StartNode = start;
            CountStart = 1;
            CountEnd = 1;
            EndNode = end;
            Edge = 1;
            Date = date;
        }

        public bool IsNewNode(string Start, string End)
        {
            if (Start == StartNode)
            {
                Edge++;
                return false;
            }
            return true;
        }

        public bool IsEqual(Node node)
        {
            var dateAccurate = Math.Abs((Date - node.Date).TotalSeconds);
            if (dateAccurate == 0)
            {
                return false;
            }
            if (StartNode == node.StartNode && EndNode == node.EndNode && dateAccurate < 2)
            {
                CountStart++;
                CountEnd++;
                Edge++;
                return true;
            }
            if (StartNode != node.StartNode && EndNode == node.StartNode && dateAccurate < 2)
            {
                CountEnd++;
                Edge++;
                return false;
            }
            if (StartNode != node.StartNode && StartNode == node.EndNode && dateAccurate < 2)
            {
                CountStart++;
                Edge++;
                return false;
            }
            if (StartNode != node.StartNode && StartNode == node.EndNode && dateAccurate < 2)
            {
                CountStart++;
                return false;
            }
            if (StartNode != node.StartNode && EndNode == node.StartNode && dateAccurate < 2)
            {
                CountEnd++;
                return false;
            }
            return false;
        }
    }
}
