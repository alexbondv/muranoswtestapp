﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter File Name:");
            var fileName = Console.ReadLine() + ".txt";
            var nodes =  FileManager.ReadFile(fileName);
            var res = NodeManager.GroupNodes(nodes);
        
            var groupedNodes = nodes.GroupBy(x => new DateTime
            (
               x.Date.Year, x.Date.Month,
                 x.Date.Day,  x.Date.Hour,  x.Date.Minute, x.Date.Second /12)
            );
            foreach (var node in groupedNodes)
            {
                foreach (var currentNode in node)
                {
                    Console.WriteLine($"{currentNode.StartNode}({currentNode.CountStart}) = {currentNode.Edge} = {currentNode.EndNode}({currentNode.CountEnd}) {currentNode.Date}");
                }
            }
        }
    }
}
