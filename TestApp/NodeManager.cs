﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public static class NodeManager
    {
        private static List<Node> _nodes { get; set; }
        static NodeManager()
        {
            _nodes = new List<Node>();
        }

        public static List<Node> GroupNodes(List<Node> nodes)
        {
            _nodes = nodes;
            for (int i = 0; i < nodes.Count(); i++)
            {
                for (int j = i+1; j < nodes.Count(); j++)
                {
                    var temp = nodes[i].Date - nodes[j].Date;
                    var temp1 = temp.TotalSeconds;
                    var dateAccurate = Math.Abs(temp1);
                    if (nodes[i].IsEqual(nodes[j]))
                    {
                        nodes.RemoveAt(j);
                        j --;
                    }
                    else { break; }
                }
            }
            return nodes;
        }
    }
}
